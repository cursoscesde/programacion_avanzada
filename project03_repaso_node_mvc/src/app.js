import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import userRoutes from './routes/UserRoutes.js';
const app = express()
const port = process.env.PORT || 3000
const mongoAtlasUrl = "mongodb+srv://cursoscesde:cursoscesde2021@cluster0.ajy33.mongodb.net/pejelagartodb?retryWrites=true&w=majority"
app.use(express.json())
app.use(cors({ origin: true }))
// routes
app.use('/api',userRoutes);

app.listen(port, async () => {
  try {
    await mongoose.connect(mongoAtlasUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  }
  catch (e) {
    console.log("Error de conexión a la DB")
  }
  console.log(`Example app listening at http://localhost:${port}`)
})