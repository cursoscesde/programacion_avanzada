import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import LoginPage from './auth/pages/login/LoginPage';
import RegisterPage from './auth/pages/register/RegisterPage';
import UsersPage from './auth/pages/users/UsersPage';
import HeaderComponent from './shared/components/header/HeaderComponent';
function App() {
  return (
      <Router>
        <HeaderComponent />
        <Switch>
          <Route path="/login" exact>
            <LoginPage />
          </Route>
          <Route path="/register" exact>
            <RegisterPage/>
          </Route>
          <Route path="/users" exact>
            <UsersPage/>
          </Route>
        </Switch>
      </Router>
  );
}

export default App;
